#ifndef GRAPHICSITEMS
#define GRAPHICSITEMS

#include <QGraphicsLineItem>
#include <QGraphicsTextItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>
#include <QString>
#include <QPainter>
#include <QDebug>
#include <qmath.h>

#include "constants.h"

using namespace std;

// See here: http://www.walletfox.com/course/customqgraphicslineitem.php

class ArrowLineItem: public QObject, public QGraphicsLineItem
{
    Q_OBJECT
public:
    ArrowLineItem(double x1, double y1, double x2, double y2, int nodeId, int linkId, double cutStart = 0, double cutEnd = 0):
        QGraphicsLineItem(x1,y1,x2,y2), _selectionOffset(10), _nodeId(nodeId), _linkId(linkId), _isTraceSelected(false), _isLinkSelected(false),
        _unselectedCol(LINK_UNSELECTED_COL), _selectedCol(LINK_SELECTED_COL), _traceSelectedCol(TRACE_SELECTED_COL),
        _unselectedWid(LINK_UNSELECTED_WID), _selectedWid(LINK_SELECTED_WID), _traceSelectedWid(TRACE_SELECTED_WID)
    {
        setFlags(QGraphicsItem::ItemIsSelectable);
        // create subline
        QLineF cLine = line();
        double factorStart = cutStart / cLine.length();
        double factorEnd   = cutEnd   / cLine.length();
        _subLine = QLineF(cLine.pointAt(0.0+factorStart), cLine.pointAt(1.0-factorEnd));
        createSelectionPolygon();
    }

    enum { Type = UserType + 1 };
    int type() const { return Type; }

    QRectF boundingRect() const
    {
        return _selectionPolygon.boundingRect();
    }

    QPainterPath shape() const
    {
        QPainterPath ret;
        ret.addPolygon(_selectionPolygon);
        return ret;
    }

    void setColors(QColor unselectedCol, QColor selectedCol, QColor traceSelectedCol) {
        _unselectedCol = unselectedCol;
        _selectedCol = selectedCol;
        _traceSelectedCol = traceSelectedCol;
    }

    void setWidths(int unselectedWid, int selectedWid, int traceSelectedWid) {
        _unselectedWid = unselectedWid;
        _selectedWid = selectedWid;
        _traceSelectedWid = traceSelectedWid;
    }

    void paint( QPainter* aPainter,
                const QStyleOptionGraphicsItem* aOption,
                QWidget* aWidget /*= nullptr*/ )
    {
        Q_UNUSED( aOption );
        Q_UNUSED( aWidget );

        qreal lineAngle = _subLine.angle();
        QLineF head1 = _subLine;
        head1.setAngle(lineAngle+32);
        QLineF head2 = _subLine;
        head2.setAngle(lineAngle-32);

        QPen pen;
        pen.setCosmetic(true);
        pen.setStyle(Qt::SolidLine);

        int size;

        if(_isLinkSelected) {
            pen.setColor(_selectedCol);
            pen.setWidth(_selectedWid);
            size = _selectedWid;
        } else {
            if(_isTraceSelected) {
                pen.setColor(_traceSelectedCol);
                pen.setWidth(_traceSelectedWid);
                size = _traceSelectedWid;
            } else {
                pen.setColor(_unselectedCol);
                pen.setWidth(_unselectedWid);
                size = _unselectedWid;
            }
        }

        head1.setLength( size * 4 );
        head2.setLength( size * 4 );

        aPainter->setPen( pen );
        aPainter->drawLine( _subLine );
        aPainter->drawLine( head1 );
        aPainter->drawLine( head2 );
    }

    void setLabel(QString label) {
        _label = new QGraphicsTextItem(label, this);
        QPointF center = boundingRect().center();
        _label->setPos(center.x() - (_label->boundingRect().width()  / 2.0),
                       center.y() - (_label->boundingRect().height() / 2.0));
    }

    void setTraceSelected(bool selected) {
        _isTraceSelected = selected;
        update();
    }
    void setLinkSelected(bool selected) {
        _isLinkSelected = selected;
        update();
    }

signals:
    void mousePressedEvent(int, int, bool);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        qDebug() << "[ArrowLineItem] Link clicked / Node" << _nodeId << "link" << _linkId << _isTraceSelected << _isLinkSelected;
        bool mod = (event->modifiers() == Qt::ShiftModifier);
        emit mousePressedEvent(_nodeId, _linkId, mod);
    }

private:
    QGraphicsTextItem* _label;
    QLineF _subLine;
    qreal _selectionOffset;
    QPolygonF _selectionPolygon;
    int _nodeId;
    int _linkId;
    bool _isTraceSelected;
    bool _isLinkSelected;

    QColor _unselectedCol, _selectedCol, _traceSelectedCol;
    int _unselectedWid, _selectedWid, _traceSelectedWid;

    void createSelectionPolygon() {
        QPolygonF nPolygon;
        qreal pi = 3.141592653589793238463;
        qreal radAngle = line().angle()* pi / 180;
        qreal dx = _selectionOffset * sin(radAngle);
        qreal dy = _selectionOffset * cos(radAngle);
        QPointF offset1 = QPointF(dx, dy);
        QPointF offset2 = QPointF(-dx, -dy);
        nPolygon << line().p1() + offset1
                 << line().p1() + offset2
                 << line().p2() + offset2
                 << line().p2() + offset1;
        _selectionPolygon = nPolygon;
        update();
    }
};

class WaypointItem: public QGraphicsEllipseItem
{
public:
    WaypointItem(double x, double y, int id=0, double radius=10):
        QGraphicsEllipseItem(x-radius, y-radius, radius*2, radius*2), _center(QPointF(x,y)), _radius(radius), _id(id),
        _unselectedCol(LINK_UNSELECTED_COL), _selectedCol(TRACE_SELECTED_COL)
    {
        setBrush(QBrush(QColor(0,0,0)));
        setPen(Qt::NoPen);
        _label = new QGraphicsTextItem(QString::number(id), this);
        double scale = 1.5*_radius / _label->boundingRect().height();
        _label->setScale(scale);
        _label->setDefaultTextColor(QColor(255,255,255));
        _label->setPos(_center.x()-(scale*_label->boundingRect().width()/2.0),
                       _center.y()-(scale*_label->boundingRect().height()/2.0));
    }

    enum { Type = UserType + 2 };

    int type() const { return Type; }
    void setTraceSelected(bool selected) {
        if(selected){
            setBrush(QBrush(_selectedCol));
        } else {
            setBrush(QBrush(_unselectedCol));
        }
        update();
    }
    void setColors(QColor unselectedCol, QColor selectedCol) {
        _unselectedCol = unselectedCol;
        _selectedCol   = selectedCol;
    }

    QPointF getCenter() const { return _center; }
    void setId(int id) {
        _id = id;
        _label->setPlainText(QString::number(id));
    }

protected:
    QPointF _center;
    QPointF _startDrag;
    double _radius;
    int _id;
    QColor _unselectedCol, _selectedCol;
    QGraphicsTextItem * _label;
};

#endif // GRAPHICSITEMS
