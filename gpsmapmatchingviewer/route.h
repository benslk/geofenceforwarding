#ifndef ROUTE_H
#define ROUTE_H

#include <QMap>
#include <QPointF>
#include <QList>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsEllipseItem>
#include <QDebug>
#include <QBrush>
#include <QColor>

#include "/usr/local/include/proj_api.h"

#include "customscene.h"
#include "graphicsitems.h"

class Route; // forward declaration
class RouteItem; // forward declaration
class RoutePointItem: public WaypointItem
{
public:
    RoutePointItem(double x, double y, double radius):
        WaypointItem(x,y,radius)
    {
        setFlags(QGraphicsItem::ItemIsSelectable|
                 QGraphicsItem::ItemIsMovable);
    }
    void setParent(RouteItem * parent) { _parent = parent; }

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event) {
        _startDrag = pos();
        QGraphicsItem::mousePressEvent(event);
    }
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    QPointF _startDrag;
    RouteItem * _parent;
};


class RouteItem
{
public:
    RouteItem(Route * parent, int id, RoutePointItem * start, RoutePointItem * end):
       _parent(parent), _id(id), _start(start), _end(end)
    {
        start->setParent(this);
        end->setParent(this);
        _routeGraphic = NULL;
    }
    RouteItem(Route * parent, int id, RoutePointItem * start):
        _parent(parent), _id(id), _start(start), _end(NULL)
    {
        start->setParent(this);
        _routeGraphic = NULL;
    }

    QGraphicsItemGroup * getRouteGraphic() { return _routeGraphic; }
    void destroyRouteGraphic();
    Route * getParent() { return _parent; }
    void setParent(Route * parent) { _parent = parent; }
    RoutePointItem * getStart() { return _start; }
    RoutePointItem * getEnd()   { return _end; }
    void setStart(RoutePointItem * start) {
        _start = start;
        start->setParent(this);
    }
    void setEnd(RoutePointItem * end) {
        _end = end;
        _end->setParent(this);
    }
    int getId() { return _id; }

    int computeRoute();
    QGraphicsItemGroup *drawRoute();

private:
    QList<QPointF> _coordinates;
    QGraphicsItemGroup * _routeGraphic;
    RoutePointItem * _start;
    RoutePointItem * _end;
    int _id;
    Route * _parent;
};


class Route: public QObject
{
    Q_OBJECT
public:
    Route(CustomScene * scene);

    CustomScene * getScene() { return _scene; }

public slots:
    void onMousePressed(QGraphicsSceneMouseEvent* event);

private:
    static int idGenerator;
    RouteItem * _currentRouteItem;
    QMap<int, RouteItem *> _routes;
    CustomScene * _scene;
};

#endif // ROUTE_H
