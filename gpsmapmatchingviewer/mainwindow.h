#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "traceloader.h"
#include "shapefileloader.h"
#include "customscene.h"
#include "route.h"
#include "traceattaributes.h"
#include "traceexaminer.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

signals:
    void mousePressedEvent(QGraphicsSceneMouseEvent*);

private:
    Ui::MainWindow* ui;
    QString _projIn, _projOut;
    TraceLoader* _traceLoader;
    ShapefileLoader* _shapeLoader;
    TraceAttributes* _traceAttr;
    TraceExaminer* _traceExam;
    QMenu* _traceMenu;
    CustomScene* _scene;
    Route* _route;
    int _routeCounter;
    bool _showShapefile;

private slots:
    void openTrace();
    void openShapefile();
    void openTraceAttributes();
    void addRoute();
    void onMousePressed(QGraphicsSceneMouseEvent*);
    void showShapefile();
};

#endif // MAINWINDOW_H
