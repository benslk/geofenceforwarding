#include "traceattaributes.h"
#include "ui_traceattaributes.h"

#include "progressdialog.h"
#include "constants.h"

#include <QString>
#include <QMap>
#include <QGraphicsItem>

TraceAttributes::TraceAttributes(QWidget *parent, CustomScene * scene, TraceLoader * traceLoader) :
    QDockWidget(parent),
    ui(new Ui::TraceAttaributes),
    _scene(scene),
    _traceLoader(traceLoader)
{
    ui->setupUi(this);

    ui->tableWidget->setRowCount(_traceLoader->getNodes().size()+1);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0,40);
    ui->tableWidget->setColumnWidth(1,40);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    // set the header
    ui->tableWidget->horizontalHeader()->setVisible(true);

    QTableWidgetItem * itemHeaderTrace = new QTableWidgetItem(true);
    itemHeaderTrace->setText("Show");
    ui->tableWidget->setHorizontalHeaderItem(0,itemHeaderTrace);

    QTableWidgetItem * itemHeaderMapMatching = new QTableWidgetItem(true);
    itemHeaderMapMatching->setText("Trace");
    ui->tableWidget->setHorizontalHeaderItem(1,itemHeaderMapMatching);

    QTableWidgetItem * itemHeaderNodeId = new QTableWidgetItem(true);
    itemHeaderNodeId->setText("Map Matching");
    ui->tableWidget->setHorizontalHeaderItem(2,itemHeaderNodeId);

    // Add the nodes in the tablewidget
    int j = 0;
    for(auto nodeId: _traceLoader->getNodes()) {
        QTableWidgetItem * item = new QTableWidgetItem(QString::number(nodeId));
        QTableWidgetItem * itemCheckBoxTrace = new QTableWidgetItem(true);
        QTableWidgetItem * itemCheckBoxMatching = new QTableWidgetItem(true);
        itemCheckBoxTrace->data(Qt::CheckStateRole);
        itemCheckBoxTrace->setCheckState(Qt::Unchecked);
        itemCheckBoxMatching->data(Qt::CheckStateRole);
        itemCheckBoxMatching->setCheckState(Qt::Unchecked);
        ui->tableWidget->setItem(j,0,itemCheckBoxTrace);
        ui->tableWidget->setItem(j,1,itemCheckBoxMatching);
        ui->tableWidget->setItem(j,2,item);
        j++;
    }

    connect(ui->tableWidget, SIGNAL(itemChanged(QTableWidgetItem*)), SLOT(showTrace(QTableWidgetItem*)));
}

TraceAttributes::~TraceAttributes()
{
    delete ui;
}

void TraceAttributes::getVisibleNodes(QList<int>* list)
{
    for(auto it = _itemTraceGraphics.begin(); it != _itemTraceGraphics.end(); ++it) {
        if(it.value()->isVisible()) {
            list->append(it.key());
        }
    }
}

void TraceAttributes::showTrace(QTableWidgetItem* item)
{
    int nodeId = _traceLoader->getNodes().at(item->row());

    if (item->column() == 0) {
        // compute the trace adnd show the trace on the graphics scene
        if (item->checkState() == Qt::Checked)
        {
            if(!_itemTraceGraphics.contains(nodeId)) {
                TraceGraphics* traceGraphics = _traceLoader->getTraceGraphics(nodeId);
                connect(traceGraphics, SIGNAL(traceSelected(int,int,bool)), this, SLOT(onSelectedTrace(int,int,bool)));
                _itemTraceGraphics.insert(nodeId, traceGraphics);
                _scene->addItem(traceGraphics->draw());
            } else {
                _itemTraceGraphics[nodeId]->setVisible(true);
            }
        }
        else if (item->checkState() == Qt::Unchecked)
        {
            if(_itemTraceGraphics.contains(nodeId)) {
                _itemTraceGraphics[nodeId]->setVisible(false);
            }
        }
        // notify of the change
        emit changedVisibleNodes();
    }
    else if (item->column() == 1)
    {
        // compute the map matching corresponding to the selected trace
        // and show the computed map matching on the graphics scene
        if (item->checkState() == Qt::Checked)
        {
            if(!_itemMapMatching.contains(nodeId)) {
                MapMatching * mapMatching = new MapMatching(_traceLoader);

                // compute the map matching for the selected trace (a new thread is launched)
                ProgressDialog * progressDiag = new ProgressDialog((QWidget *)parent(), "Computing Map Matching for node "+QString::number(nodeId));
                connect(mapMatching, &MapMatching::loadProgressChanged, progressDiag, &ProgressDialog::updateProgress);
                _itemMapMatching.insert(nodeId, mapMatching);
                mapMatching->computeMapMatching(nodeId);
                progressDiag->exec();

                // Draw the map matching on the scene
                QGraphicsItemGroup * mapMatchingGraphic = mapMatching->draw();
                connect(mapMatching, SIGNAL(mapMatchingSelected(int,int,bool)), this, SLOT(onSelectedMapMatching(int,int,bool)));
                _scene->addItem(mapMatchingGraphic);
            }
            else
            {
                _itemMapMatching[nodeId]->setVisible(true);
            }
        }
        else if (item->checkState() == Qt::Unchecked)
        {
            if(_itemMapMatching.contains(nodeId)) {
                _itemMapMatching[nodeId]->setVisible(false);
            }
        }
    }
}

void TraceAttributes::onSelectedTrace(int nodeId, int linkId, bool mod)
{
    qDebug() << "[TraceAttributes] trace selected" << nodeId;
    emit selectTrace(nodeId, linkId, mod);
}

void TraceAttributes::onSelectedMapMatching(int nodeId, int subMapMatchingId, bool mod)
{
    QPair<int, int> mm = qMakePair(nodeId, subMapMatchingId);
    if(_selectedMapMatching.contains(mm) && mod) {
        // remove the map matching from the selection
        _itemMapMatching[nodeId]->setSelectedSubMapMatching(subMapMatchingId, false);
        _selectedMapMatching.removeOne(mm);
    } else {
        if(!mod) {
            // clear all selections
            for(QPair<int,int> p: _selectedMapMatching) {
                _itemMapMatching[p.first]->setSelectedSubMapMatching(p.second, false);
            }
            _selectedMapMatching.clear();
        }

        // add the new selection
        _itemMapMatching[nodeId]->setSelectedSubMapMatching(subMapMatchingId, true);
        _selectedMapMatching.append(mm);
    }
}

void TraceAttributes::setSelectedLink(int nodeId, int linkId, bool selected)
{
    _itemTraceGraphics.value(nodeId)->setSelectedLink(linkId, selected);
}
