#ifndef SHAPEFILELOADER_H
#define SHAPEFILELOADER_H

#include <QString>
#include <QGraphicsScene>
#include <QtConcurrent>
#include <QGraphicsItemGroup>
#include <ogrsf_frmts.h>

#include "progressdialog.h"
#include "loader.h"

class ShapefileLoader: public Loader
{
public:
    ShapefileLoader(MainWindow* parent = 0, QString filename="", QString projIn="", QString projOut=""):
        Loader(parent, filename), _projIn(projIn), _projOut(projOut) {}
    ~ShapefileLoader();

    void load();
    void setVisible(bool visible) { _shapefileGraphic->setVisible(visible); }
    QGraphicsItemGroup * drawShapefile();

private:
    bool concurrentLoad();

    QString _projIn;
    QString _projOut;
    QList<OGRGeometry *> _shapes;
    QGraphicsItemGroup * _shapefileGraphic;
};

#endif // SHAPEFILELOADER_H
