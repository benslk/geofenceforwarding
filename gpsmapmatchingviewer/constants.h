#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>
#include <QColor>

const int MAX_LOCATIONS_MAP_MATCHING = 1000;
const int WAYPOINT_SIZE = 5;
const QString PROJ_IN  = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
const QString PROJ_OUT = "+proj=utm +zone=29 +ellps=GRS80 +units=m +no_defs";
const QColor ORANGE = QColor(255, 145, 0);
const QColor GREEN  = QColor(21, 148, 25);
const QColor PURPLE = QColor(153, 14, 240);
const QColor BLUE   = QColor(0, 17, 255);
const QColor RED    = QColor(255, 14, 0);
const QColor YELLOW = QColor(237, 237, 24);

// colors taken from http://flatuicolors.com
const QColor TRACE_SELECTED_COL  = QColor(231, 76, 60);
const QColor LINK_SELECTED_COL   = QColor(153, 14, 240);
const QColor LINK_UNSELECTED_COL = QColor(52, 73, 94);
const QColor TRACE_LINK_JOINT_COL = QColor(52, 152, 219);

const int    TRACE_SELECTED_WID  = 3;
const int    LINK_SELECTED_WID   = 5;
const int    LINK_UNSELECTED_WID = 2;

const QColor SHAPEFILE_COL       = QColor(127, 140, 141);
const int    SHAPEFILE_WID       = 2;

const QColor MATCH_SELECTED_COL  = QColor(22, 160, 133);
const QColor MATCH_LINK_SELECTED_COL = QColor(153, 14, 240);
const QColor MATCH_LINK_UNSELECTED_COL = QColor(243, 156, 18);
const QColor MATCH_LINK_JOINT_COL = QColor(230, 126, 34);

#endif // CONSTANTS_H

