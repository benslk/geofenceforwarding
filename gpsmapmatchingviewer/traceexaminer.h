#ifndef TRACEEXAMINER_H
#define TRACEEXAMINER_H

#include <QDockWidget>

#include "traceattaributes.h"

namespace Ui {
class TraceExaminer;
}

class TraceExaminer : public QDockWidget
{
    Q_OBJECT

public:
    explicit TraceExaminer(QWidget *parent = 0, TraceAttributes * traceAttr = 0);
    ~TraceExaminer();

private slots:
    void changedSelectedNode(QString);
    void changedVisibleNodes();
    void onSelectedTrace(int nodeId, int linkId, bool mod);
    void selectLink(const QItemSelection &, const QItemSelection &);

private:
    void displayNodeInfo(int nodeId);
    void selectTrace(int nodeId, int linkId, bool mod);
    void clearSelectedLinks();

    Ui::TraceExaminer *ui;
    TraceAttributes* _traceAttr;
    int _currentSelectedNode = -1;
    QSet<QPair<int,int> > _currentSelectedLinks;
};

#endif // TRACEEXAMINER_H
