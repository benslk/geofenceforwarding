#include "traceloader.h"
#include <QDebug>
#include <QString>
#include <QPen>
#include <QPair>

#include "osrmwrapper.h"
#include "graphicsitems.h"

void TraceLoader::load()
{
    _loadResult = QtConcurrent::run(this, &TraceLoader::concurrentLoad);
}

TraceGraphics* TraceLoader::getTraceGraphics(int nodeId)
{
    TraceGraphics* traceGraphics = new TraceGraphics(nodeId);
    QPair<double, double> prevPoint;
    int prevTS = -1;
    int counter = 0;
    for(auto it = _nodes.value(nodeId)->begin(); it != _nodes.value(nodeId)->end(); ++it) {
        // draw the point
        int curTS = it.key();
        QPointF p = it.value();

        if(prevTS != -1) {
            // draw the line between the two points
            ArrowLineItem* lineItem = new ArrowLineItem(p.x(), p.y(), prevPoint.first, prevPoint.second, nodeId, counter-1, WAYPOINT_SIZE, WAYPOINT_SIZE);

            // display the timestamp difference on the line
            lineItem->setLabel(QString::number(curTS - prevTS));
            traceGraphics->addLine(lineItem);
        }

        // Display the waypoint
        WaypointItem* pointItem = new WaypointItem(p.x(), p.y(), counter, WAYPOINT_SIZE);
        traceGraphics->addWayPoint(pointItem);

        prevPoint = qMakePair(p.x(),p.y());
        prevTS = curTS;
        counter++;
    }

    return traceGraphics;

}

bool TraceLoader::concurrentLoad()
{
    _nodes.clear();

    QFile * file = new QFile(_filename);

    if(!file->open(QFile::ReadOnly | QFile::Text))
    {
        return false;
    }
    int lineNbr = 0;
    while(!file->atEnd())
    {
        QString line = QString(file->readLine()).split(QRegExp("[\r\n]"), QString::SkipEmptyParts).at(0);
        QStringList fields = line.split(_delimiter);
        double lon = fields.at(8).toDouble();
        double lat = fields.at(9).toDouble();
        QDateTime timestamp = QDateTime::fromMSecsSinceEpoch(fields.at(0).toLongLong() / 1000);
        int nodeId = fields.at(5).toInt();
        // Create the node map if not already created
        if(!_nodes.contains(nodeId)) {
            _nodes.insert(nodeId, new QMap<uint,QPointF>());
        }
        // add the timestamp and corresponding position
        double x, y;
        OSRMWrapper::getInstance().transformCoordinates(lat, lon, &x, &y);
        QPointF p(x,y);
        _nodes.value(nodeId)->insert(timestamp.toTime_t(), p);

        // Indicate the load progress of the file
        if(lineNbr % 1000 == 0) {
            qreal loadProgress = 1.0 - file->bytesAvailable() / (qreal)file->size();
            emit loadProgressChanged(loadProgress);
        }
        lineNbr++;
    }
    qDebug() << "loaded" << QFileInfo(_filename).fileName() << "with" << lineNbr << "lines";
    emit loadProgressChanged((qreal) 1.0);

    return true;
}
