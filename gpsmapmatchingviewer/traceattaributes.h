#ifndef TRACEATTARIBUTES_H
#define TRACEATTARIBUTES_H

#include "traceloader.h"
#include "customscene.h"
#include "mapmatching.h"
#include "tracegraphics.h"

#include <QDebug>
#include <QDockWidget>
#include <QTableWidgetItem>

namespace Ui {
class TraceAttaributes;
}

class TraceAttributes : public QDockWidget
{
    Q_OBJECT

public:
    explicit TraceAttributes(QWidget* parent = 0, CustomScene* scene = 0, TraceLoader* traceLoader = 0);
    ~TraceAttributes();

    void getVisibleNodes(QList<int>* list);
    QMap<uint, QPointF>* getNodeTrace(int nodeId) {
        return _traceLoader->getNodeTrace(nodeId);
    }

    void setSelected(int nodeId, bool selected) {
        if(_itemTraceGraphics.contains(nodeId)) {
            _itemTraceGraphics[nodeId]->setSelectedTrace(selected);
        }
        if(_itemMapMatching.contains(nodeId)) {
            _itemMapMatching[nodeId]->setSelected(selected);
        }
    }

public slots:
    void setSelectedLink(int nodeId, int linkId, bool selected);

signals:
    void changedVisibleNodes();
    void selectTrace(int nodeId, int linkId, bool mod);

private slots:
    void showTrace(QTableWidgetItem*);
    void onSelectedTrace(int nodeId, int linkId, bool mod);
    void onSelectedMapMatching(int nodeId, int subMapMatchingId, bool mod);

private:
    Ui::TraceAttaributes* ui;
    CustomScene* _scene;
    TraceLoader* _traceLoader;
    QMap<int, TraceGraphics*> _itemTraceGraphics;
    QMap<int, MapMatching*> _itemMapMatching;
    QList<QPair<int,int>> _selectedMapMatching;
    int _signalRecieved = 0;
};

#endif // TRACEATTARIBUTES_H
