#ifndef TRACEGRAPHICS_H
#define TRACEGRAPHICS_H

#include "graphicsitems.h"
#include "constants.h"

#include <QDebug>

class TraceGraphicsGroup: public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
public:
    TraceGraphicsGroup(int id):
        QGraphicsItemGroup(), _id(id) {}

private:
    int _id;
};

class TraceGraphics: public QObject
{
    Q_OBJECT
public:
    TraceGraphics(int id):
        _id(id) {}

    void addLine(ArrowLineItem* line) {
        _itemLineGraphics.append(line);
    }

    void addWayPoint(WaypointItem* wayPoint) {
        _itemWayPointGraphics.append(wayPoint);
    }

    void setVisible(bool visible) {
        if(visible != _isVisible) {
            for(int i = 0; i < _itemLineGraphics.size(); ++i) {
                _itemLineGraphics.at(i)->setVisible(visible);
            }
            for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
                _itemWayPointGraphics.at(i)->setVisible(visible);
            }
        }
        _isVisible = visible;
    }

    bool isVisible() { return _isVisible; }

    void setSelectedTrace(bool selected) {
        for(int i = 0; i < _itemLineGraphics.size(); ++i) {
            _itemLineGraphics.at(i)->setTraceSelected(selected);
        }
        for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
            _itemWayPointGraphics.at(i)->setTraceSelected(selected);
        }
    }

    void setSelectedLink(int linkId, bool selected) {
        if(linkId >= 0 && linkId < _itemLineGraphics.size()) {
            _itemLineGraphics.at(linkId)->setLinkSelected(selected);
        }
    }

    QGraphicsItemGroup* draw() {
        QGraphicsItemGroup* traceGroup = new QGraphicsItemGroup();
        traceGroup->setHandlesChildEvents(false);

        for(int i = 0; i < _itemLineGraphics.size(); ++i) {
            ArrowLineItem* line = _itemLineGraphics.at(i);
            connect(line, SIGNAL(mousePressedEvent(int,int,bool)), this, SLOT(onlinkSelected(int,int,bool)));
            traceGroup->addToGroup(line);
        }
        for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
            traceGroup->addToGroup(_itemWayPointGraphics.at(i));
        }

        return traceGroup;
    }

signals:
    void traceSelected(int nodeId, int linkId, bool mod);

private slots:
    void onlinkSelected(int nodeId, int linkId, bool mod) {
        qDebug() << "[Tracegraphics] link selected" << nodeId << "link" << linkId;
        emit traceSelected(nodeId, linkId, mod);
    }

private:
    QList<ArrowLineItem*> _itemLineGraphics;
    QList<WaypointItem*>  _itemWayPointGraphics;

    int _id;
    bool _isVisible = true;
};

#endif // TRACEGRAPHICS_H
