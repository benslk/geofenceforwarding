#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphicsview.h"
#include "progressdialog.h"
#include "traceattaributes.h"
#include "constants.h"
#include "traceexaminer.h"

#include <QSettings>
#include <QString>
#include <QFileDialog>
#include <QStandardPaths>
#include <QGraphicsView>
#include <QGraphicsItemGroup>
#include <QMessageBox>
#include <QLayout>
#include <QMenu>
#include <QProgressBar>


MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _projIn  = PROJ_IN;
    _projOut = PROJ_OUT;
    _routeCounter = -1;
    _showShapefile = false;
    _traceAttr = NULL;
    _traceExam = NULL;

    _scene = new CustomScene();
    _route = new Route(_scene);
    GraphicsView* v = new GraphicsView(_scene, this);
    setCentralWidget(v);

    connect(ui->actionOpenShapefile, SIGNAL(triggered(bool)), this, SLOT(openShapefile()));
    connect(ui->actionOpenTrace, SIGNAL(triggered(bool)), this, SLOT(openTrace()));
    connect(_scene, SIGNAL(mousePressedEvent(QGraphicsSceneMouseEvent*)), this, SLOT(onMousePressed(QGraphicsSceneMouseEvent*)));
    connect(this, SIGNAL(mousePressedEvent(QGraphicsSceneMouseEvent*)), _route, SLOT(onMousePressed(QGraphicsSceneMouseEvent*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openTrace()
{
    QSettings settings;
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open a trace",
                                                    settings.value("defaultTracePath", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString(),
                                                    tr("Trace file (*.csv *.txt);;All files (*.*)"));
    if(filename.isEmpty()) {
        return;
    }
    // Save the filename path in the app settings
    settings.setValue("defaultTracePath", QFileInfo(filename).absolutePath());

    _traceLoader = new TraceLoader(this, filename, ",");
    ProgressDialog* progressDiag = new ProgressDialog(this, "Loading "+QFileInfo(filename).fileName());
    connect(_traceLoader, &TraceLoader::loadProgressChanged, progressDiag, &ProgressDialog::updateProgress);
    _traceLoader->load();
    progressDiag->exec();

    // Display the "Trace attributes" button in the menu bar
    _traceMenu = new QMenu();
    _traceMenu->setTitle("Trace");
    QAction* actionOpen_traceAttributes = _traceMenu->addAction("Show trace attributes");
    connect(actionOpen_traceAttributes, SIGNAL(triggered(bool)), this, SLOT(openTraceAttributes()));
    ui->menuBar->addMenu(_traceMenu);
}

void MainWindow::openShapefile()
{
    QSettings settings;
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open a Shapefile",
                                                    settings.value("defaultShapefilePath", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString(),
                                                    tr("Shapefile (*.shp)"));
    if(filename.isEmpty()) {
        return;
    }
    // Save the filename path in the app settings
    settings.setValue("defaultShapefilePath", QFileInfo(filename).absolutePath());

    _shapeLoader = new ShapefileLoader(this, filename, _projIn, _projOut);
    ProgressDialog* progressDiag = new ProgressDialog(this, "Loading "+QFileInfo(filename).fileName());
    connect(_shapeLoader, &ShapefileLoader::loadProgressChanged, progressDiag, &ProgressDialog::updateProgress);
    _shapeLoader->load();
    progressDiag->exec();

    // Draw the shape on the scene
    QGraphicsItemGroup  *groupItem = _shapeLoader->drawShapefile();
    _scene->addItem(groupItem);
    _showShapefile = true;

    QMenu * menu = new QMenu();
    menu->setTitle("Shapefile");
    QAction* actionOpen_showShapefile = menu->addAction("Show shapefile");
    connect(actionOpen_showShapefile, SIGNAL(triggered(bool)), this, SLOT(showShapefile()));

    QAction* actionOpen_addRoute = menu->addAction("Add Route");
    connect(actionOpen_addRoute, SIGNAL(triggered(bool)), this, SLOT(addRoute()));

    ui->menuBar->addMenu(menu);
}

void MainWindow::openTraceAttributes()
{
    if(!_traceAttr) {
        _traceAttr = new TraceAttributes(this, _scene, _traceLoader);
    } else {
        _traceAttr->setVisible(true);
    }
    if(!_traceExam) {
        _traceExam = new TraceExaminer(this, _traceAttr);
    } else {
        _traceExam->setVisible(true);
    }

    addDockWidget(Qt::RightDockWidgetArea, _traceAttr);
    addDockWidget(Qt::RightDockWidgetArea, _traceExam);
    tabifyDockWidget(_traceAttr, _traceExam);
}

void MainWindow::addRoute()
{
    // reinitilialize the route conter
    _routeCounter = 0;
}

void MainWindow::onMousePressed(QGraphicsSceneMouseEvent* event)
{
    if(_routeCounter != -1 && _routeCounter < 2) {
        // call the route manager
        emit mousePressedEvent(event);
        _routeCounter++;
    }
}

void MainWindow::showShapefile()
{
    _showShapefile = !_showShapefile;
    _shapeLoader->setVisible(_showShapefile);
}
