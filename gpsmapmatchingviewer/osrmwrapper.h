#ifndef OSRMWRAPPER_H
#define OSRMWRAPPER_H

#include "../library/osrm.hpp"
#include "../util/git_sha.hpp"
#include "../util/json_renderer.hpp"
#include "../util/routed_options.hpp"
#include "../util/simple_logger.hpp"

#include <osrm/json_container.hpp>
#include <osrm/libosrm_config.hpp>
#include <osrm/route_parameters.hpp>

#include "/usr/local/include/proj_api.h"

#include "constants.h"
#include "mapmatching.h"

#include <QPointF>
#include <QList>
#include <QMap>


class OSRMWrapper
{
public:
    static OSRMWrapper& getInstance()
    {
        static OSRMWrapper instance; // Guaranteed to be destroyed.
                                     // Instantiated on first use.
        return instance;
    }

    void computeRoute(QList<QPointF>* coordinates, QPointF start, QPointF end);
    void computeMapMatching(QMap<uint, QPointF>* waypoints, QList<SubMapMatching*>* subMapMatching, int mapMatchingId);

    void revertCoordinates(double x, double y, double* lat, double* lon);
    void transformCoordinates(double lat, double lon, double* x, double* y);

private:
    OSRMWrapper();
    OSRMWrapper(OSRMWrapper const&)     = delete;
    void operator=(OSRMWrapper const&)  = delete;

    OSRM* _routingMachine;
    projPJ _projIn, _projOut;
};

#endif // OSRMWRAPPER_H
