#include "route.h"

#include "osrmwrapper.h"

int Route::idGenerator = 1;

void RoutePointItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
//    QPointF currentPos = _center + (pos() - _startDrag);
    _center += (pos() - _startDrag);
    _startDrag = pos();
    // calculate the route and show the new route
    _parent->destroyRouteGraphic();
    bool ret = _parent->computeRoute();
    if(ret == 0) {
        QGraphicsItemGroup * routeGraphic = _parent->drawRoute();
        _parent->getParent()->getScene()->addItem(routeGraphic);
    }
    QGraphicsItem::mouseMoveEvent(event);
}

void RoutePointItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    _center += (pos() - _startDrag);
    // calculate the route and show the new route
    _parent->destroyRouteGraphic();
    bool ret = _parent->computeRoute();
    if(ret == 0) {
        QGraphicsItemGroup * routeGraphic = _parent->drawRoute();
        _parent->getParent()->getScene()->addItem(routeGraphic);
    }
    QGraphicsItem::mouseReleaseEvent(event);
}


Route::Route(CustomScene *scene) :
    _scene(scene)
{
    _currentRouteItem = NULL;
}

void Route::onMousePressed(QGraphicsSceneMouseEvent *event)
{
    QPointF pt = event->scenePos();
    RoutePointItem * item = new RoutePointItem(pt.x(), pt.y(), 25);
    _scene->addItem(item);

    if(!_currentRouteItem) {
        item->setId(1);
        _currentRouteItem = new RouteItem(this, idGenerator++, item);
    } else {
        item->setId(2);
        _currentRouteItem->setEnd(item);
        _routes.insert(_currentRouteItem->getId(), _currentRouteItem);
        _currentRouteItem->computeRoute();
        QGraphicsItemGroup * routeGraphic = _currentRouteItem->drawRoute();
        _scene->addItem(routeGraphic);
        _currentRouteItem = NULL;
    }
}


void RouteItem::destroyRouteGraphic() {
    if(_routeGraphic) {
        _parent->getScene()->removeItem(_routeGraphic);
        delete _routeGraphic;
        _routeGraphic = NULL;
    }
}

int RouteItem::computeRoute()
{
    qDebug() << "compute the route" << _id;
    _coordinates.clear();
    if(!_end)
        return -1;

    QPointF start = _start->getCenter();
    QPointF end = _end->getCenter();

    OSRMWrapper::getInstance().computeRoute(&_coordinates, start, end);
    if(_coordinates.size() > 0)
        return 0;
    return -1;
}

QGraphicsItemGroup *RouteItem::drawRoute()
{
    // draw the route from the coordinate points
    QGraphicsItemGroup * groupItems = new QGraphicsItemGroup();
    if(_coordinates.size() <= 0) {
        return groupItems;
    }

    QPen pen = QPen(QColor(0,0,0));
    pen.setWidth(3);
    pen.setCosmetic(true);

    QPointF prevPoint = _coordinates.front();
    for(int i = 1; i < _coordinates.size(); ++i) {
        QPointF curPoint = _coordinates.at(i);
        // draw the line between prevPoint and curPoint
        QGraphicsLineItem * lineItem = new QGraphicsLineItem(prevPoint.x(), prevPoint.y(), curPoint.x(), curPoint.y());
        lineItem->setPen(pen);
        groupItems->addToGroup(lineItem);
        prevPoint = curPoint;
    }
    _routeGraphic = groupItems;

    return groupItems;
}
