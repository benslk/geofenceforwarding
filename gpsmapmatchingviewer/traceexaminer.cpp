#include "traceexaminer.h"
#include "ui_traceexaminer.h"

#include <QDebug>
#include <QItemSelection>

TraceExaminer::TraceExaminer(QWidget *parent, TraceAttributes * traceAttr) :
    QDockWidget(parent),
    ui(new Ui::TraceExaminer)
{
    ui->setupUi(this);
    _traceAttr = traceAttr;
    connect(_traceAttr, SIGNAL(changedVisibleNodes()), this, SLOT(changedVisibleNodes()));
    connect(_traceAttr, SIGNAL(selectTrace(int,int,bool)), this, SLOT(onSelectedTrace(int,int,bool)));

    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    // add all of the traces that are visible in the trace attributes to the combo box
    QList<int> visibleNodes;
    _traceAttr->getVisibleNodes(&visibleNodes);
    ui->comboBox->addItem("");
    for(int & traceId: visibleNodes) {
        ui->comboBox->addItem(QString::number(traceId));
    }

    connect(ui->comboBox,
            SIGNAL(currentIndexChanged(QString)),
            this,
            SLOT(changedSelectedNode(QString)));

    connect(ui->tableWidget->selectionModel(),
            SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
            this,
            SLOT(selectLink(const QItemSelection &, const QItemSelection &)));
}

TraceExaminer::~TraceExaminer()
{
    delete ui;
}

void TraceExaminer::changedSelectedNode(QString id)
{
    qDebug() << "[Combobox/TraceExaminer] selected trace" << id;
    if(id.isEmpty()) {
        // unselect all the links and trace
        clearSelectedLinks();
        _traceAttr->setSelected(_currentSelectedNode, false);
        _currentSelectedNode = -1;
        ui->tableWidget->setVisible(false);
    } else {
        selectTrace(id.toInt(), -1, false);
    }
}

void TraceExaminer::onSelectedTrace(int nodeId, int linkId, bool mod)
{
    qDebug() << "[Click/TraceExaminer] selected trace" << nodeId;

    //!\\ will call the slot changedSelectedNode
    ui->comboBox->setCurrentText(QString::number(nodeId));
    selectTrace(nodeId, linkId, mod);
}

void TraceExaminer::selectLink(const QItemSelection & selected, const QItemSelection & deselected)
{
    // select the new links
    for(int i = 0; i < selected.indexes().size(); ++i) {
        QModelIndex selectedIndex = selected.indexes().at(i);
        ui->tableWidget->selectRow(selectedIndex.row());
        _traceAttr->setSelectedLink(_currentSelectedNode, selectedIndex.row(), true);
        _currentSelectedLinks.insert(qMakePair(_currentSelectedNode,selectedIndex.row()));
    }
    // unselect the old links
    for(int i = 0; i < deselected.indexes().size(); ++i) {
        QModelIndex deselectedIndex = deselected.indexes().at(i);
        _traceAttr->setSelectedLink(_currentSelectedNode, deselectedIndex.row(), false);
        _currentSelectedLinks.remove(qMakePair(_currentSelectedNode,deselectedIndex.row()));
        // unselect the entire row
        for(int j = 0; j < ui->tableWidget->columnCount(); ++j) {
            QModelIndex idx = ui->tableWidget->model()->index(deselectedIndex.row(),j);
            ui->tableWidget->selectionModel()->select(idx, QItemSelectionModel::Deselect);
        }
    }
}

void TraceExaminer::changedVisibleNodes()
{
    int oldCurrentSelectedNode = _currentSelectedNode;

    // update the combo box
    ui->comboBox->clear();
    QList<int> visibleNodes;
    _traceAttr->getVisibleNodes(&visibleNodes);
    ui->comboBox->addItem("");
    for(int & traceId: visibleNodes) {
        ui->comboBox->addItem(QString::number(traceId));
    }

    // select the node
    if(oldCurrentSelectedNode != -1) {
        int index = ui->comboBox->findText(QString::number(oldCurrentSelectedNode));
        qDebug() << "current selected node" << oldCurrentSelectedNode << "combo box index" << index;
        if(index != -1) {
            ui->comboBox->setCurrentIndex(index);
            _currentSelectedNode = oldCurrentSelectedNode;
        } else {
            _traceAttr->setSelected(oldCurrentSelectedNode, false);
            _currentSelectedNode = -1;
        }
        // remove links that were selected
        clearSelectedLinks();
    }
}

void TraceExaminer::displayNodeInfo(int nodeId)
{
    QMap<uint, QPointF>* trace = _traceAttr->getNodeTrace(nodeId);

    ui->tableWidget->setVisible(true);
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setRowCount(trace->size()-1);
    for(int i = 1; i < trace->keys().size(); ++i) {
        uint    prevTS  = trace->keys().at(i-1);
        uint    curTS   = trace->keys().at(i);
        QPointF prevPos = trace->values().at(i-1);
        QPointF curPos  = trace->values().at(i);
        QLineF  line(prevPos, curPos);

        double distance = line.length();
        int    duration = curTS - prevTS;
        double speed    = distance / duration;

        QTableWidgetItem* itemDistance = new QTableWidgetItem(QString::number(distance));
        QTableWidgetItem* itemSpeed    = new QTableWidgetItem(QString::number(speed));
        QTableWidgetItem* itemDuration = new QTableWidgetItem(QString::number(duration));
        QTableWidgetItem* itemStart    = new QTableWidgetItem(QString::number(prevTS));
        QTableWidgetItem* itemEnd      = new QTableWidgetItem(QString::number(curTS));

        ui->tableWidget->setItem(i-1, 0, itemDistance);
        ui->tableWidget->setItem(i-1, 1, itemDuration);
        ui->tableWidget->setItem(i-1, 2, itemSpeed);
        ui->tableWidget->setItem(i-1, 3, itemStart);
        ui->tableWidget->setItem(i-1, 4, itemEnd);
    }
}

void TraceExaminer::selectTrace(int nodeId, int linkId, bool mod)
{
    int oldCurrentSelectedNode = _currentSelectedNode;

    if(oldCurrentSelectedNode != nodeId) {
        // different node as the one previously selected
        if(oldCurrentSelectedNode != -1) {
            // unselect the previously selected trace (if valid)
            _traceAttr->setSelected(oldCurrentSelectedNode, false);
        }
        _traceAttr->setSelected(nodeId, true);
        displayNodeInfo(nodeId);
        _currentSelectedNode = nodeId;

        clearSelectedLinks();
        if(linkId != -1) {
            // add the selected link (if valid)
            _currentSelectedLinks.insert(qMakePair(nodeId, linkId));
            ui->tableWidget->selectRow(linkId);
//            QModelIndex idx = ui->tableWidget->model()->index(linkId,0);
//            ui->tableWidget->selectionModel()->select(idx, QItemSelectionModel::Select);
        }
    } else if(linkId != -1) {
        // same node as the one selected
        if(!mod) {
            clearSelectedLinks();
        }
         // add the clicked link to the current selection
        _currentSelectedLinks.insert(qMakePair(nodeId, linkId));
        _traceAttr->setSelectedLink(nodeId, linkId, true);
        ui->tableWidget->selectRow(linkId);
//        QModelIndex idx = ui->tableWidget->model()->index(linkId,0);
//        ui->tableWidget->selectionModel()->select(idx, QItemSelectionModel::Select);
    }
}

void TraceExaminer::clearSelectedLinks()
{
    // unselect all the selected cells
    ui->tableWidget->clearSelection();
    // unselect the previously selected links
    for(QPair<int,int> x: _currentSelectedLinks) {
        _traceAttr->setSelectedLink(x.first, x.second, false);
    }
    // clear the selected link set
    _currentSelectedLinks.clear();
}
