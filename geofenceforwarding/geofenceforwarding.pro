#-------------------------------------------------
#
# Project created by QtCreator 2015-08-19T13:23:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = geofenceforwarding
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS = -mmacosx-version-min=10.8 -std=gnu0x -stdlib=libc+

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_system

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_system.a


unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_filesystem

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_filesystem.a

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_thread-mt

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_thread-mt.a

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_program_options

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_program_options.a

unix: LIBS += -L$$PWD/../../osrm-backend/build/ -lOSRM

INCLUDEPATH += $$PWD/../../osrm-backend/include $$PWD/../../osrm-backend/third_party
DEPENDPATH += $$PWD/../../osrm-backend/include $$PWD/../../osrm-backend/third_party

unix: PRE_TARGETDEPS += $$PWD/../../osrm-backend/build/libOSRM.a


