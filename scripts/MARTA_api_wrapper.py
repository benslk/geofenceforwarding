
#!/usr/bin/env python

from __future__ import division
import urllib2
import json
from datetime import datetime
from pytz import timezone
import re
import time

#Base URL for MARTA API
BASE_URL = 'http://developer.itsmarta.com/BRDRestService/BRDRestService.svc/GetAllBus'
TIME_PATTERN = re.compile('^\d+\/\d+\/\d{4}\s\d+\:\d+\:\d+\s[AP]M$')
TIMEZONE = timezone('US/Eastern')
UNACTIVE_TIMEOUT = 240 # 4 minutes of unactivity for the buses
NB_MAX_REQ = 10

class Vehicle(object):
    """ Represents a vehicle """
    def __init__(self, vid, ts, lat, lon, rt, tr, st):
        super(Vehicle, self).__init__()
        self.vid = vid
        self.ts  = ts
        self.lat = lat
        self.lon = lon
        self.rt  = rt
        self.tr  = tr
        self.st  = st
        self.last_visit = ts

    def __str__(self):
        return "Vehicle #%s Route #%s (%s, %s, %s)" % (self.vid,self.rt,self.lat,self.lon,self.ts)

    def to_file(self):
        return "%s;%s;%s;%s;%s;%s;%s\n" % (self.ts,self.vid,self.lat,self.lon,self.rt,self.tr,self.st)

    def is_updated(self, veh, time):
        """ Returns true if "veh" is an update of the current vehicle """
        self.last_visit = time
        if(veh.vid != self.vid or veh.rt != self.rt):
            return False
        return (veh.ts != self.ts and veh.lat != self.lat and veh.lon != self.lon)

    def get_time_since_last_visit(self, time):
        """ Returns the duration since the last visit: time - self.last_visit """
        return (time - self.last_visit)

def convert_time(s):
    if(TIME_PATTERN.match(s)):
        return datetime.strptime(s, "%m/%d/%Y %H:%M:%S %p").replace(tzinfo=TIMEZONE)
    else:
        return None

def get_time():
	return datetime.now(TIMEZONE)

def to_timestamp(dt):
    """ Returns the timestamp corresponding to the datetime "dt" """
    return int((dt - datetime(1970, 1, 1, tzinfo=TIMEZONE)).total_seconds())

def send_request():
    nb_req = 0
    while nb_req < NB_MAX_REQ:
        try:
            data = json.load(urllib2.urlopen(BASE_URL))
        except Exception as e:
			print e
			nb_req += 1
			time.sleep(max(1,int(nb_req/3)))
			continue
        else:
			return data
    return None

def get_all_vehicles():
	""" Returns all active vehicles in a dictionary indexed by the vid """
	buses = send_request()
	vehicles = {}
	for bus in buses:
		vid = bus['VEHICLE']
		rt  = bus['ROUTE']
		ts  = to_timestamp(convert_time(bus['MSGTIME']))
		lat = bus['LATITUDE']
		lon = bus['LONGITUDE']
		st  = bus['STOPID']
		tr  = bus['TRIPID']
		# Add the vehicle to the "vehicles" dictionary
		vehicles[vid] = Vehicle(vid, ts, lat, lon, rt, tr, st)

	return vehicles
