from __future__ import division
import xml.etree.ElementTree as ET
import urllib2
import random
from datetime import datetime
import re
from multiprocessing import Pool
import math
import sys
import time
from CTA_keys import KEYS

BASE_URL = "http://www.ctabustracker.com/bustime/api/v1/"
NB_MAX_REQ = len(KEYS) * 2
TIME_MIN_PATTERN = re.compile('^\d{8}\s\d{2}\:\d{2}$')
TIME_SEC_PATTERN = re.compile('^\d{8}\s\d{2}\:\d{2}\:\d{2}$')
MAX_ROUTE_LIMIT = 10
ERROR_MSG = "Invalid API access key supplied"
UNACTIVE_TIMEOUT = 240 # 4 minutes of unactivity for the buses

class Vehicle(object):
    """ Represents a vehicle """
    def __init__(self, vid, ts, lat, lon, rt):
        super(Vehicle, self).__init__()
        self.vid = vid
        self.ts  = ts
        self.lat = lat
        self.lon = lon
        self.rt  = rt
        self.last_visit = ts

    def __str__(self):
        return "Vehicle #%s Route #%s (%s, %s, %s)" % (self.vid,self.rt,self.lat,self.lon,self.ts)

    def to_file(self):
        return "%s;%s;%s;%s;%s\n" % (self.ts,self.vid,self.lat,self.lon,self.rt)

    def is_updated(self, veh, time):
        """ Returns true if "veh" is an update of the current vehicle """
        self.last_visit = time
        if(veh.vid != self.vid or veh.rt != self.rt):
            return False
        return (veh.ts != self.ts and veh.lat != self.lat and veh.lon != self.lon)

    def get_time_since_last_visit(self, time):
        """ Returns the duration since the last visit: time - self.last_visit """
        return (time - self.last_visit)

def test_api_keys():
    keys_error = []
    for key in KEYS:
        url = BASE_URL + "gettime?key=%s" % (key)
        print "[TESTING] " + key + " [" + url + "]"
        xml = urllib2.urlopen(url).read()
        root = ET.fromstring(xml)
        error = root.find('error')
        if(error is not None):
            print "[ERROR] " + key + " [" + error.find('msg').text + "]"
            keys_error.append(key)
        else:
            print "[PASSED] " + key + " [" + root.find("tm").text + "]"

    return keys_error

def get_client():
 		# userless access
 		idx = random.randint(0, len(KEYS)-1)
 		return "key=%s" % (KEYS[idx])

def send_request(base, params = {}):
	""" Forms the request to send to the API and returns the data if no errors occured """
	nb_req = 0
	while nb_req < NB_MAX_REQ:
		full_url = base + "?" + "&".join(["%s=%s" % (k,v.replace(' ','+')) for (k,v) in params.items()])
		if(len(params) > 0):
			full_url += "&" + get_client()
		else:
			full_url += get_client()

		try:
		    xml = urllib2.urlopen(full_url).read()
		    tree = ET.fromstring(xml)
		    nb_children = len(tree)
		    errors = tree.findall('error')
		    if(errors and len(errors) == nb_children):
		        msg = errors[0].find('msg').text
		        if(msg == ERROR_MSG):
		            raise Exception(msg)
		        else:
		            return tree
		except Exception as e:
			print e
			nb_req += 1
			time.sleep(min(1,nb_req/10))
			continue
		else:
			return tree
	return None

def print_routes():
    root = send_request(BASE_URL+"getroutes")
    if(root is None):
        return

    route_counter = 0
    print root.tag
    for route in root.iter("route"):
        print "%s\t%s" % (route.find('rt').text, route.find('rtnm').text)
        route_counter += 1

    print "Displayed %d routes" % route_counter

def print_buses_route(route_id):
    time = get_time()
    root = send_request(BASE_URL+"getvehicles", {"rt": route_id})
    if(root is None):
        return

    bus_counter = 0
    for bus in root.iter("vehicle"):
        timestamp = convert_time(bus.find('tmstmp').text)
        time_diff = get_time_diff(timestamp, time)
        print "%s\t%s\t%s\t%s" % (bus.find('vid').text, bus.find('des').text, timestamp, time_diff)
        bus_counter += 1

    print "Displayed %d buses for route %s" % (bus_counter, route_id)

def get_time():
    """ returns the current time,
        represented as "YYYYMMDD HH:MM:SS" """

    root = send_request(BASE_URL+"gettime")
    if(root is None):
        return

    date = root.find("tm").text
    return convert_time(date)

def convert_time(s):
    if(TIME_MIN_PATTERN.match(s)):
        return datetime.strptime(s, "%Y%m%d %H:%M")
    elif(TIME_SEC_PATTERN.match(s)):
        return datetime.strptime(s, "%Y%m%d %H:%M:%S")
    else:
        return None

def to_timestamp(dt):
    """ Returns the timestamp corresponding to the datetime "dt" """
    return int((dt - datetime(1970, 1, 1)).total_seconds())

def get_time_diff(a, b):
    c = b-a
    return divmod(c.days * 86400 + c.seconds, 60)

def get_routes():
    """ Returns the list of active? route identifiers ('rt') """
    root = send_request(BASE_URL+"getroutes")
    if(root is None):
        return []

    routes = []
    for route in root.iter("route"):
        routes.append(route.find('rt').text)

    return routes

def get_vehicles(rt):
    """ Returns the list of vehicles for the list of routes 'rt' """

    root = send_request(BASE_URL+"getvehicles", {"rt": ",".join(r for r in rt)})
    if(root is None):
        return

    vehicles = {}
    for veh in root.iter("vehicle"):
        timestamp = convert_time(veh.find('tmstmp').text)
        # time_diff = get_time_diff(timestamp, time)
        vid = veh.find('vid').text
        ts  = to_timestamp(timestamp)
        lat = veh.find('lat').text
        lon = veh.find('lon').text
        rt  = veh.find('rt').text
        # Add the vehicle to the "vehicles" dictionary
        vehicles[vid] = Vehicle(vid, ts, lat, lon, rt)

    return vehicles

def get_all_vehicles():
    """ Returns all active vehicles in a dictionary indexed by the vid """
    # get the routes
    routes = get_routes()

    # get the number of processes to retrive
    nb_process = int(math.ceil(len(routes) / MAX_ROUTE_LIMIT))
    # divide the routes into route "chunks"
    chunks = [routes[x:x+int(MAX_ROUTE_LIMIT)] for x in range(0, len(routes), int(MAX_ROUTE_LIMIT))]

    # start the multiprocessing
    p = Pool(processes=nb_process)
    res = p.map(get_vehicles, chunks)
    p.terminate()
    p.join()

    # Put together all the vehicles
    return dict((vid, veh) for d in res for (vid, veh) in d.items())
