import schedule
import time

def job():
    global previous_time
    now = time.time()
    print("I'm working... %s seconds" % (now - previous_time))
    previous_time = now

schedule.every(10).seconds.do(job)
schedule.every().hour.do(job)
schedule.every().day.at("10:30").do(job)
previous_time = time.time()

while 1:
    schedule.run_pending()
    time.sleep(1)
